#include "filter.h"

#include <QImage>


Filter::Filter()
{

}

QImage Filter::overlayMask(const QImage &img,const QImage &mask)
{
    if(img.isNull() || mask.isNull())
        return QImage();

    QImage outImg = QImage(img.size(), QImage::Format_RGB32);

    outImg.fill(Qt::black);

    auto imgBits = reinterpret_cast<const QRgb*>(img.constBits());
    auto maskBits = reinterpret_cast<const QRgb*>(mask.constBits());
    auto outBits = reinterpret_cast<QRgb*>(outImg.bits());

    int imgLen = img.width() * img.height();
    int maskLen = mask.width() * mask.height();

    for(int index = 0; index < qMin(imgLen, maskLen); ++index){
        if(!qAlpha(maskBits[index])){
            outBits[index] = imgBits[index];
        }
    }

    return outImg;
}

QImage Filter::handleImage(const QImage &img, bool invert)
{

    if(img.isNull())
        return QImage();

    QImage outImg(img.size(), QImage::Format_Grayscale8);

    outImg.fill(Qt::white);

    QImage m_redImg = selectChannel(img, Filter::Red);
    QImage m_greenImg = selectChannel(img, Filter::Green);
    QImage m_blueImg = selectChannel(img, Filter::Blue);


    auto redBits = m_redImg.constBits();
    auto blueBits = m_blueImg.constBits();
    auto greenBits = m_greenImg.constBits();


    auto outBits = outImg.bits();


    const int size = img.width() * img.height();

    for(int index = 0; index < size; ++index)
    {
        if(invert){

            outBits[index] = burn(redBits[index], greenBits[index]);

        }else{

            uchar M = greenBits[index];
            uchar I = substract(greenBits[index], redBits[index]);

            outBits[index] = linearBurn(M, I);
        }
    }



    return outImg;

}

QImage Filter::thresholdImage(const QImage &img, int val)
{

    if(img.isNull())
        return QImage();

    QImage outImg(img.size(), QImage::Format_Grayscale8);

    outImg.fill(Qt::black);

    auto srcBits = img.constBits();

    auto outBits = outImg.bits();


    const int size = img.width() * img.height();

    for(int index = 0; index < size; ++index)
    {
        if(srcBits[index]>=val)
            outBits[index] = srcBits[index] ? 255 : 0;
    }

    return outImg;
}

QImage Filter::coloredImage(const QImage &img)
{
    if(img.isNull())
        return QImage();

    QImage outImg(img.size(), QImage::Format_RGB32);

    outImg.fill(Qt::black);

    auto srcBits = img.constBits();
    auto outBits = reinterpret_cast<QRgb*>(outImg.bits());

    const int size = img.width() * img.height();

    for(int index = 0; index < size; ++index)
    {
        if(srcBits[index])
            outBits[index] = QColor(Qt::yellow).rgb();
    }


    return outImg;
}

uchar Filter::burn(uchar M, uchar I, bool inverse) const
{
    if(inverse)
        std::swap(M, I);

    return qMax(255 - (256 * (255 - I) / (M + 1)), 0);
}

uchar Filter::linearBurn(uchar M, uchar I) const
{
    return qMax(M + I - 255, 0);
}

uchar Filter::multiply(uchar M, uchar I) const
{
    return M * I / 255;
}

uchar Filter::substract(uchar M, uchar I, bool inverse) const
{
    if(inverse)
        std::swap(M, I);

    return qMax(M - I, 0);
}

uchar Filter::difference(uchar M, uchar I) const
{
    return abs(M - I);
}

uchar Filter::grainExtract(uchar M, uchar I, bool inverse) const
{
    if(inverse)
        std::swap(M, I);

    return qMax(qMin(M - I + 127, 255), 0);
}

uchar Filter::grainMerge(uchar M, uchar I) const
{
    return qMax(qMin(M + I - 127, 255), 0);
}

uchar Filter::softLight(uchar M, uchar I, bool inverse) const
{
    if(inverse)
        std::swap(M, I);

    uchar Rs = (255 - ((255 - I) * (255 - M))) / 256;

    return (((255 - I) * M * I) + (I * Rs)) / 256;
}

uchar Filter::screen(uchar M, uchar I) const
{
    return 0;
}
