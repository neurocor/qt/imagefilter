#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QGraphicsView>

class QGraphicsPixmapItem;
class Group;

class ImageViewer : public QGraphicsView
{
    Q_OBJECT
public:
    ImageViewer(QWidget *parent =nullptr);
    virtual ~ImageViewer();


    inline QImage filtredImage() const
    {
        return m_filterImg;
    }

    QImage sourceIimage() const;
    QImage image() const;

    void changeOpacity(float val);

    void setSourceImage(const QImage &img);
    void setImage(const QImage &img);


    void setFilterImg(const QImage &newFilterImg);

protected:

    virtual void resizeEvent(QResizeEvent *e)override;

private:

    QGraphicsPixmapItem *m_srcItem;
    QGraphicsPixmapItem *m_pixItem;
    Group *m_lineage;

    QImage m_filterImg;
};

#endif // IMAGEVIEWER_H
