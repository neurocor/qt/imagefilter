QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DESTDIR = ../$$TARGET

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    customellipse.cpp \
    filter.cpp \
    group.cpp \
    imageviewer.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    customellipse.h \
    filter.h \
    group.h \
    imageviewer.h \
    widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32:Release:QMAKE_POST_LINK += windeployqt --qmldir $$[QT_INSTALL_QML] $$DESTDIR
