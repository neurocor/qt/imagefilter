#include "imageviewer.h"

#include <QGraphicsPixmapItem>

#include "group.h"

ImageViewer::ImageViewer(QWidget *parent)
    :QGraphicsView(parent)
{
    setScene(new QGraphicsScene(this));


    m_pixItem = new QGraphicsPixmapItem;
    m_srcItem = new QGraphicsPixmapItem;

    m_lineage = new Group;

    scene()->addItem(m_pixItem);
    scene()->addItem(m_srcItem);
    scene()->addItem(m_lineage);

}

ImageViewer::~ImageViewer()
{
    delete m_pixItem;
    delete m_srcItem;
}

QImage ImageViewer::sourceIimage() const
{
    return m_srcItem->pixmap().toImage();
}

QImage ImageViewer::image() const
{
    return m_pixItem->pixmap().toImage();
}

void ImageViewer::changeOpacity(float val)
{
    m_srcItem->setOpacity(val);

    update();
}

void ImageViewer::setSourceImage(const QImage &img)
{
    m_srcItem->setPixmap(QPixmap::fromImage(img));

    update();
}

void ImageViewer::setImage(const QImage &img)
{
    m_pixItem->setPixmap(QPixmap::fromImage(img));

    update();
}

void ImageViewer::resizeEvent(QResizeEvent *e)
{
    QGraphicsView::resizeEvent(e);

    auto tmpImg = m_pixItem->pixmap();

    scene()->setSceneRect(tmpImg.rect());

    fitInView(tmpImg.rect(), Qt::KeepAspectRatio);

}

void ImageViewer::setFilterImg(const QImage &newFilterImg)
{
    setImage(m_filterImg = newFilterImg);
}
