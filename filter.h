#ifndef FILTER_H
#define FILTER_H

#include "qimage.h"
class QImage;

class Filter
{
public:

    enum Channel{
        Blue,
        Green,
        Red,
        Alpha
    };

    Filter();

    static inline Filter &instance(){
        static Filter filter;
        return filter;
    }




    QImage overlayMask(const QImage &img, const QImage &mask);

    QImage handleImage(const QImage &img, bool invert);
    QImage thresholdImage(const QImage &img, int val);
    QImage coloredImage(const QImage &img);

    uchar burn(uchar M, uchar I, bool inverse = false) const;
    uchar linearBurn(uchar M, uchar I) const;
    uchar multiply(uchar M, uchar I) const;
    uchar substract(uchar M, uchar I, bool inverse = false) const;
    uchar difference(uchar M, uchar I) const;
    uchar grainExtract(uchar M, uchar I, bool inverse) const;
    uchar grainMerge(uchar M, uchar I) const;
    uchar softLight(uchar M, uchar I, bool inverse  = false) const;
    uchar screen(uchar M, uchar I) const;

    inline QImage selectChannel(const QImage &source, Filter::Channel channel)
    {
        if(source.isNull())
            return QImage();

        QImage outImage(source.size(), QImage::Format_Grayscale8);

        auto srcBits = reinterpret_cast<const QRgb*>(source.constBits());
        auto outBits = outImage.bits();

        const double size = source.width() * source.height();

        for(int index = 0; index < size;++index)
        {
            outBits[index] = (srcBits[index] >> (channel * 8)) & 0xFF;
        }

        return outImage;

    }

private:

};



#endif // FILTER_H
