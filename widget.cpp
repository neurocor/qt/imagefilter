#include "widget.h"

#include <QApplication>
#include <QVBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QSlider>
#include <QLabel>
#include <QFileDialog>

#include "imageviewer.h"
#include "filter.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    auto mainLay = new QVBoxLayout(this);

    auto loadBtn = new QPushButton("Load image", this);
    auto saveBtn = new QPushButton("Save image", this);
    auto invertCheckBox = new QCheckBox("Invert image", this);

    auto imgViewer = new ImageViewer(this);

    auto threasholdSlider=new QSlider(Qt::Horizontal, this);
    auto opacitySlider=new QSlider(Qt::Horizontal, this);


    threasholdSlider->setRange(0, 255);
    opacitySlider->setRange(0, 100);

    imgViewer->changeOpacity(0);

    mainLay->addWidget(loadBtn);
    mainLay->addWidget(invertCheckBox);
    mainLay->addWidget(threasholdSlider);
    mainLay->addWidget(opacitySlider);
    mainLay->addWidget(saveBtn);
    mainLay->addWidget(imgViewer);

    connect(loadBtn, &QPushButton::clicked,[imgViewer,threasholdSlider, invertCheckBox]{

        auto imgName = QFileDialog::getOpenFileName();

        if(imgName.isEmpty())
            return;

        imgViewer->setSourceImage(QImage(imgName));
        imgViewer->setImage(QImage(imgName));

        auto inImg = imgViewer->sourceIimage();

        QImage filtImg = Filter::instance().handleImage(inImg, invertCheckBox->isChecked());
        QImage thrImg = Filter::instance().thresholdImage(filtImg, threasholdSlider->value());
        QImage outImg = Filter::instance().coloredImage(thrImg);

        imgViewer->setFilterImg(filtImg);


        imgViewer->setImage(outImg);


    });

    connect(invertCheckBox, &QCheckBox::toggled,[imgViewer, threasholdSlider](bool invert){
        auto inImg = imgViewer->sourceIimage();

        QImage filtImg = Filter::instance().handleImage(inImg, invert);
        QImage thrImg = Filter::instance().thresholdImage(filtImg, threasholdSlider->value());
        QImage outImg = Filter::instance().coloredImage(thrImg);

        imgViewer->setFilterImg(filtImg);


        imgViewer->setImage(outImg);
    });

    connect(saveBtn, &QPushButton::clicked,[imgViewer]{

        auto imgName = QFileDialog::getSaveFileName();

        if(imgName.isEmpty())
            return;

        imgViewer->image().save(imgName);

    });

    connect(threasholdSlider, &QSlider::valueChanged,[imgViewer](int val){
        auto inImg = imgViewer->filtredImage();

        QImage thrImg = Filter::instance().thresholdImage(inImg, val);
        QImage outImg = Filter::instance().coloredImage(thrImg);

        imgViewer->setImage(outImg);
    });

    connect(opacitySlider, &QSlider::valueChanged,[imgViewer](int val){


        imgViewer->changeOpacity(val / 100.f);
    });
}

Widget::~Widget()
{
}

